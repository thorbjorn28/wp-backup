#!/bin/bash
# Thorbjorn Axelsson thax@thax.work
# http://www.thax.work

# Modify these variables for your system
WP_ROOT="/usr/share/wordpress"
WP_CONFIG="/etc/wordpress/wp-config.php"
BACKUP_DIR="/root/wordpress_backups"

# gzip set by default due to low RAM/CPU usage, xz compresses better
TAR_COMPRESSION="z"     # z=gzip, J=xz, j=bzip2
TAR_SUFFIX="tgz"        # Should match compression alg: tgz, tar.xz, tar.bz

## Do not edit below this line ##

WP_BASE=`echo ${WP_ROOT%/*}`
WP_DIR=`basename $WP_ROOT`

[ -d $WP_BASE/$WP_DIR ] || \
    { (>&2 echo "$WP_BASE/$WP_DIR is not accessible. Exiting."); exit 1; }
[ -d $BACKUP_DIR ] || \
    { (>&2 echo "$BACKUP_DIR is not accessible. Exiting."); exit 1; }
[ -f $WP_CONFIG ] || \
    { (>&2 echo "$WP_CONFIG is not accessible. Exiting.") ; exit 1; }

DB_HOST=`fgrep DB_HOST $WP_CONFIG | awk -F\' '{ print $4 }'`
DB_NAME=`fgrep DB_NAME $WP_CONFIG | awk -F\' '{ print $4 }'`
DB_USER=`fgrep DB_USER $WP_CONFIG | awk -F\' '{ print $4 }'`
DB_PASS=`fgrep DB_PASS $WP_CONFIG | awk -F\' '{ print $4 }'`

DATE=`date --iso-8601=date`

WP_DB_ARCHIVE="wp-db_$DATE.gz" 
mysqldump -h $DB_HOST -u $DB_USER -p${DB_PASS} $DB_NAME | \
    gzip > $BACKUP_DIR/$WP_DB_ARCHIVE || \
    { (>&2 echo "mysqldump failed"); exit 1; }

WP_FILES_ARCHIVE="wp-files_$DATE.$TAR_SUFFIX"
tar c${TAR_COMPRESSION}f $BACKUP_DIR/$WP_FILES_ARCHIVE -C $WP_BASE $WP_DIR ||\
    { (>&2 echo "tar WP files failed"); exit 1; }


