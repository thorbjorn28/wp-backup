# Wordpress Backup bash script

## Overview
This is a simple bash script for archiving the current Wordpress configuration
(currently excluding wp-config.php as it contains the database password). 

There are many similar scripts out there, but after looking at a couple I found
that they were not to my liking, so I wrote my own.

## Usage

1. Edit the variables (should be self-explanatory) at the top of the file.
2. Run it. 

If it succeeds, there should be no output. Error messages are printed if 
something fails.

## Author
Thorbjorn Axelsson <thax@thax.work>

